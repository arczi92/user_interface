﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Server_User_Interface.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult GetXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            string path = HttpContext.Server.MapPath("~/App_Data/controlsTest.xml");
            xmlDoc.Load(path);

            return Content(xmlDoc.OuterXml, "text/xml");
        }
    }
}