﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Server_User_Interface.Startup))]
namespace Server_User_Interface
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
