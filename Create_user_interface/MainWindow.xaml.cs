﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;

namespace Create_user_interface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

       
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(Url_Texbox.Text != string.Empty)
            { 
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(@Url_Texbox.Text);
                XmlNodeList ControlsNodesList = xmlDoc.DocumentElement.SelectNodes("/CONTROLS/*");

                string id_control, caption, key, defaultValue;


                foreach (XmlNode item in ControlsNodesList)
                {
                    id_control = item.Attributes["key"].Value;
                    caption = item.Attributes["caption"].Value;
                    key = item.Attributes["key"].Value;
                    defaultValue = item.Attributes["defaultValue"].Value;
                    bool defaultVal = false;

                    //   if ("CHECKBOX" == item.Name)


                    StackPanel sp = new StackPanel() { Orientation = Orientation.Horizontal, Margin = new System.Windows.Thickness(5) };
                    switch (item.Name)
                    {

                        case "TEXT":
                            sp.Children.Add(new Label() { Content = item.Attributes["caption"] });
                            sp.Children.Add(new TextBox() { Text = defaultValue, Tag = item.Attributes["key"].Value });
                            mainPanel.Children.Add(sp);
                            break;

                        case "CHECKBOX":
                            if (defaultValue == "true")
                                defaultVal = true;

                            sp.Children.Add(new CheckBox() { Content = item.Attributes["caption"], Name = "check", Tag = item.Attributes["key"].Value, IsChecked = defaultVal });
                            mainPanel.Children.Add(sp);
                            break;

                        case "LIST":
                            sp.Children.Add(new Label() { Content = item.Attributes["caption"] });
                            ListBox listbox = new ListBox() { Tag = item.Attributes["key"].Value };
                            ListBoxItem lis_box_item = new ListBoxItem();


                            var x = item.ChildNodes;

                            foreach (XmlNode ittemList in item.ChildNodes)
                            {
                                listbox.Items.Add(ittemList.Attributes["value"].Value.ToString());
                            }
                            sp.Children.Add(listbox);
                            mainPanel.Children.Add(sp);
                            listbox.SelectedValue = item.Attributes["defaultValue"].Value;
                            break;
                    }
                    
                }
            }
        }

        private void Expot_to_XML_Click(object sender, RoutedEventArgs e)
        {
            var documentx = new XDocument();
            var rootElelemtnt = new XElement("VALUES");
            string id = null;
            string value = null;


            foreach (UIElement item in mainPanel.Children)
            {
                if (item is StackPanel)
                {
                    var x = item as StackPanel;
                    foreach (var element in x.Children)
                    {
                        if (element is TextBox)
                        {
                            id = (element as TextBox).Tag.ToString();
                            value = (element as TextBox).Text;
                            rootElelemtnt.Add(new XElement(id, value));
                        }
                        if (element is CheckBox)
                        {
                            id = (element as CheckBox).Tag.ToString();
                            value = (element as CheckBox).IsChecked.ToString();
                            rootElelemtnt.Add(new XElement(id, value));
                        }
                        if (element is ListBox)
                        {
                            id = (element as ListBox).Tag.ToString();
                            var select_value = (element as ListBox).SelectedItem.ToString();

                            if (select_value != string.Empty)
                                value = select_value;
                            rootElelemtnt.Add(new XElement(id, value));
                        }
                    }
                }
            }



            StringBuilder sb = new StringBuilder();
            sb.Append("test");
            sb.Append(DateTime.Now.Year);
            sb.Append(DateTime.Now.Month);
            sb.Append(DateTime.Now.Day);
            sb.Append(DateTime.Now.Hour);
            sb.Append(DateTime.Now.Minute);
            sb.Append(".out.xml");

            MessageBoxResult result = MessageBox.Show("Czy na pewno chcesz zapisać dane do pliku xml ?", "Komunikat", MessageBoxButton.YesNo);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    rootElelemtnt.Save(sb.ToString());
                    break;
                    //case MessageBoxResult.No:
                    //    break;
            }



        }
    }
}
